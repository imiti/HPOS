// JavaScript Document
var $ = function (id) {  
    return "string" == typeof id ? document.getElementById(id) : id;  
};

//显示所有的行到表格中，这些行是从数据库中拿出来的
function showAllData()
{
	removeAllData();
	//开启SQLite数据库事务
	sqlProvider.loadTable('SELECT * FROM TEA_INFO order by id desc',[],function(result){
	   
		table._Cursor = result;
		table._pageIndex = 0;
		//_maxpage = -1;
		table.refreshfromsql();
	});	
}  

//用于移除所有的表格中的当前显示数据（它并不去除数据库记录） 
function removeAllData()
{
	 this.table.DeleteAllRows();
}

function enableButtons(flag)
{
	if(flag)
	{
	  parent._selectedrowid = datatable.rows[this.table._CurrentIndex].cells[0].innerHTML;
	  $("modify").className="addb-div2";
	$("delete").className="addb-div3";
	addEventHandler($("modify"),"click",updateclick); 
	addEventHandler($("delete"),"click",deleteclick); 
	}
	else
	{
	  $("modify").disabled="disabled";
	  $("delete").disabled="disabled";
	  removeEventHandler($("modify"),"click",updateclick); 
	  removeEventHandler($("delete"),"click",deleteclick); 
	}
}

function addclick()
{
	parent._operatetype = "add";
	 window.parent.gotopage("add"); 
}
function updateclick()
{
	parent._operatetype = "update";
	 window.parent.gotopage("add"); 
}
function deleteclick()
{
	parent._operatetype = "delete";
	 window.parent.gotopage("add"); 
}
function prev_pageclick()
{
	this.table.prevpage();
}
function next_pageclick()
{
	this.table.nextpage();
}
function returnclick()
{
	window.parent.gotopage("teachoose"); 
}
function init()
{
	if(parent._admintips!="")
	{
	sAlert(null,{width:300,height:100,left:250,top:90},parent._admintips,1); 
	}
	this.pageSize = 9;
	
	 this.sqlProvider = window.parent.sqlProvider;
	 sqlProvider = this.sqlProvider;
	
	$("modify").className="modifydisabled";
	$("delete").className="deletedisabled";
	datatable = $("ttable"); 
	this.table=new Table("ttable",{ onClick:function(){enableButtons(true);},onKeymove:function(){	enableButtons(false);},tableth:["id","商品名称","商品单价"],setpage:"cpage",tdwidth:[0,120,120],colname:["id","goods_name","goods_sellperprice"],pageSize:9}); 
	table = this.table;
	addEventHandler($("add"),"click",addclick); 
	addEventHandler($("return"),"click",returnclick); 
//	addEventHandler($("modify"),"click",updateclick); 
	//addEventHandler($("delete"),"click",deleteclick); 
	
	showAllData();
	
}

window.onload = init;