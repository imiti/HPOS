// JavaScript Document
var $ = function (id) {  
    return "string" == typeof id ? document.getElementById(id) : id;  
};

//显示所有的行到表格中，这些行是从数据库中拿出来的
function showAllData()
{
	removeAllData();
	//开启SQLite数据库事务
	this.sqlProvider.loadTable('SELECT * FROM MEMBERCARD_INFO order by id desc',[],function(result){
	 
		this.table._Cursor = result;
		this.table._pageIndex = 0;
		this._maxpage = -1;
		this.table.refreshfromsql();
	});	
}  

//用于移除所有的表格中的当前显示数据（它并不去除数据库记录） 
function removeAllData()
{
	 this.table.DeleteAllRows();
}

function enableButtons(flag)
{
	if(flag)
	{
	  parent._selectedrowid = datatable.rows[this.table._CurrentIndex].cells[0].innerHTML;
	  $("modify").disabled=false;
	  $("delete").disabled=false;
	}
	else
	{
	  $("modify").disabled="disabled";
	  $("delete").disabled="disabled";
	}
}

function addclick()
{
	parent._operatetype = "add";
	 window.parent.gotopage("addmember"); 
}
function updateclick()
{
	parent._operatetype = "update";
	 window.parent.gotopage("addmember"); 
}
function deleteclick()
{
	parent._operatetype = "delete";
	 window.parent.gotopage("addmember"); 
}

function init()
{
	try{
	 this.sqlProvider = window.parent.sqlProvider;
	}
	catch(e){
	}
	$("modify").disabled="disabled";
	$("delete").disabled="disabled";
	datatable = $("ttable"); 
	this.table=new Table("ttable",{ onClick:function(){enableButtons(true);},onKeymove:function(){	enableButtons(false);},tableth:["id","姓名","电话","积分"],setpage:"cpage",tdwidth:[0,220,220,120],colname:["id","name","tel","credits"],pageSize:9}); 
	addEventHandler($("add"),"click",addclick); 
	addEventHandler($("modify"),"click",updateclick); 
	addEventHandler($("delete"),"click",deleteclick); 

	showAllData();
}

window.onload = init;